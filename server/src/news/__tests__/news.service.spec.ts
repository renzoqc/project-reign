import { Test, TestingModule } from '@nestjs/testing';
import { HttpService, NotFoundException } from '@nestjs/common';
import { getModelToken } from '@nestjs/mongoose';

import { NewsService } from '../services/news.service';
import { New } from '../entities/new.entity';

const mockHttpService = () => ({
  get: jest.fn(),
});
const mockNewModel = () => ({
  find: jest.fn(),
  findById: jest.fn(),
  create: jest.fn(),
  findByIdAndUpdate: jest.fn(),
  findByIdAndDelete: jest.fn(),
});

describe('HackerNewsService', () => {
  let newService;
  let newModel;
  let httpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NewsService,
        {
          provide: HttpService,
          useFactory: mockHttpService,
        },
        {
          provide: getModelToken(New.name),
          useFactory: mockNewModel,
        },
      ],
    }).compile();

    newService = module.get<NewsService>(NewsService);
    newModel = module.get(getModelToken(New.name));
    httpService = module.get<HttpService>(HttpService);
  });

  it('should be defined', () => {
    expect(newService).toBeDefined();
  });

});
