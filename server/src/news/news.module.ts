import { Module, HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { NewsController } from './controllers/news.controller';
import { NewsService } from './services/news.service';
import { New, NewSchema } from './entities/new.entity';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: New.name,
        schema: NewSchema,
      },
    ]),
    HttpModule,
  ],
  controllers: [NewsController],
  providers: [NewsService],
  exports: [NewsService],
})
export class NewsModule {}
